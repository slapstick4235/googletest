package Helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.junit.jupiter.api.Assertions.*;

public class TestHelper {
	WebDriver driver;
	
	public TestHelper(WebDriver driver){
		this.driver=driver;
	}
	
	private boolean isRowOfLinksAligned(List<String> linkTexts) {
    int n = linkTexts.size();
	boolean aligned=true;
    WebElement firstElement =driver.findElement(By.linkText(linkTexts.get(0))); 
    int y= firstElement.getRect().getY();
    for(int i=1;i<n;i++) {
			WebElement e = driver.findElement(By.linkText(linkTexts.get(i)));
			if(e.getRect().getY()!=y) {
				aligned=false;
				break;
			}
    }
    return aligned;

    	
	}
	
	private boolean isColumnOfLinksAligned(List<String> linkTexts) {
    int n = linkTexts.size();
	boolean aligned=true;
    WebElement firstElement =driver.findElement(By.linkText(linkTexts.get(0))); 
    int x = firstElement.getRect().getX();
    for(int i=1;i<n;i++) {
			WebElement e = driver.findElement(By.linkText(linkTexts.get(i)));
			if(e.getRect().getX()!= x) {
				aligned=false;
				break;
			}
    }
    return aligned;

    	
	}
	public void rowOfLinksAligner(List<String> linkTexts) {
		assertTrue(isRowOfLinksAligned(linkTexts), linkTexts.toString()+" are not aligned in a row.");
	}

	public void columnOfLinksAligner(List<String> linkTexts) {
		assertTrue(isColumnOfLinksAligned(linkTexts), linkTexts.toString()+" are not aligned in a column.");
	}
	
	public void linksAligner(List<String> linkTexts) {
		assertTrue(isColumnOfLinksAligned(linkTexts)^isRowOfLinksAligned(linkTexts), linkTexts.toString()+" are not aligned.");
	}
		

    	
	
    



	
}
