package Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.Wait;

public class CSSHelper {
	WebDriver driver;
	Wait wait;
	public CSSHelper(WebDriver driver,Wait wait){
		this.driver=driver; 
		this.wait=wait;
	}

	public CSSHelper(WebDriver driver){
		this.driver=driver; 
	}
 public Color getTextColorByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 Color textColor =Color.fromString(element.getCssValue("color")); 
	 return textColor;
 }
 
 public Color getBackgroundColorByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 Color backgroundColor =Color.fromString(element.getCssValue("background-color")); 
	 return backgroundColor;
 }

 public Color getBorderColorByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 Color borderColor =Color.fromString(element.getCssValue("border-top-color")); 
	 return borderColor;
 }
 public Color getBorderColorByElement(WebElement element) {
	 Color borderColor =Color.fromString(element.getCssValue("border-top-color")); 
	 return borderColor;
 }
 
 public String getTextAlignByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 String textAlign =element.getCssValue("text-align"); 
	 return textAlign;
 }
 public String getMinWidthByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 String minWidth =element.getCssValue("min-width"); 
	 return minWidth;
 }

 public String getHeightByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 String height =element.getCssValue("height"); 
	 return height ;
 }
 public String getPaddingByName(String elementName) {
	 WebElement element = driver.findElement(By.name(elementName));
	 String padding =element.getCssValue("padding"); 
	 return padding;
 }
 
 

}
