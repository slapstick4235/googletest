package sewm.Utils;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverUtility {
	String browser = System.getProperty("browser");
    WebDriver driver;
    Wait wait;
    JavascriptExecutor js;
    Duration t = Duration.ofMillis(10000);
    Duration f =Duration.ofMillis(250);
    

    	
	public WebDriver init(String url) { 
	if(browser==null) { //use firefox if no argument is given
			browser="firefox";
		}
	
        
		switch(browser) {
		case "Firefox":
			WebDriverManager.firefoxdriver().setup();
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			firefoxOptions.addArguments("--headless", "--no-sandbox");
			driver = new FirefoxDriver(firefoxOptions);
			break;
        case "Chrome":
			WebDriverManager.chromedriver().setup();
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--headless", "--no-sandbox");
			driver = new ChromeDriver(chromeOptions);
			break;
        case "Edge":
			WebDriverManager.edgedriver().setup();
			EdgeOptions edgeOptions = new EdgeOptions();
			//edgeOptions.addArguments("--headless", "--no-sandbox"); //no add arguments method?
			edgeOptions.setCapability("headless",true); //Not sure if this would work.
			driver = new EdgeDriver(edgeOptions);
			break;
        case "IE":
			WebDriverManager.iedriver().setup();
			InternetExplorerOptions ieOptions = new InternetExplorerOptions();
			//ieOptions.addArguments("--headless", "--no-sandbox");
			ieOptions.setCapability("headless",true); //Not sure if this would work.
			driver = new InternetExplorerDriver(ieOptions);
			break;
        case "PhantomJS":
			WebDriverManager.phantomjs().setup();
			driver = new PhantomJSDriver();
			break;
        case "Opera":
			WebDriverManager.operadriver().setup();
			OperaOptions operaOptions= new OperaOptions();
			operaOptions.addArguments("--headless", "--no-sandbox");
			driver = new OperaDriver(operaOptions);
			break;
		default:
            WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
			
        }
		
		wait = new FluentWait(driver)
				.withTimeout(t)
				.pollingEvery(f)
				.ignoring(Exception.class);
     
		js = (JavascriptExecutor)driver;

		driver.get(url);
	    String size = System.getProperty("size");
		if(size==null) {
		    driver.manage().window().maximize();	
		}
		else {
			String[] S = size.split("[, ]+");
			int w = Integer.parseInt(S[0]);
			int h = Integer.parseInt(S[1]);
			driver.manage().window().setSize(new Dimension(w,h)); 
		}
		return driver;
	}
	
	public WebElement hoverOverElement(WebElement element) {
		Actions mouse = new Actions(driver);
		mouse.moveToElement(element).build().perform(); 
		return element;
	}
	
	public Wait<WebDriver> getWait() {
		return wait;
	}
	
	public WebElement scrollToSpan(String text) {
		String expression ="//span[contains(text(), \'"+text+"\')]"; 
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(expression)));
        WebElement element = driver.findElement(By.xpath(expression));
	    js.executeScript("arguments[0].scrollIntoView(true);",element);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(expression)));
	    return element;
	}
	
	public WebElement scrollToH2(String text) {
		String expression ="//h2[contains(text(), \'"+text+"\')]"; 
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(expression)));
        WebElement element = driver.findElement(By.xpath(expression));
	    js.executeScript("arguments[0].scrollIntoView(true);",element);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(expression)));
	    return element;
	}


}
