package sewm.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.common.io.Files;

public class Camera {
	WebDriver driver;
	public Camera(WebDriver driver){
		this.driver=driver;
	}
	public void screenshot(String filename) {
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 try {
         File placeHolder =new File("./screenshots/"+filename+".png") ;
			Files.createParentDirs(placeHolder);
			Files.copy(scrFile,placeHolder );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
