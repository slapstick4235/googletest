package sewm;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;

import com.deque.html.axecore.results.Rule;
import com.deque.html.axecore.selenium.AxeBuilder;

import Helpers.TestHelper;
import sewm.Utils.DriverUtility;


@ExtendWith(MyTestWatcher.class)
public class PageTest {
	WebDriver driver;
	TestHelper testHelper;
	Wait wait;
	DriverUtility utility =new DriverUtility();

	public void checkForViolations() //I am creating this to use as a super class, so that any test will have this accessibility test method.
    {

		AxeBuilder axe = new AxeBuilder();
        if(axe.analyze(driver).violationFree()==false) {
         List<Rule> violations=axe.analyze(driver).getViolations();
         int n = violations.size();
         String[] helpMessages = new String[n]; 
         for(int i=0;i<n;i++) {
        	 String x=violations.get(i).getHelp();
        	 helpMessages[i]=x;
         }
         fail(Arrays.toString(helpMessages));
        }
    }
}
