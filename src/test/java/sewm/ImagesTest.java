package sewm;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import Helpers.TestHelper;
import sewm.Pages.ImagesPage;

class ImagesTest extends PageTest {

	
	String url = "https://www.google.com";

	@BeforeEach
	void initDriver() {
		driver = utility.init(url);
		wait = utility.getWait();
		MyTestWatcher.setDriver(driver);
		this.testHelper=new TestHelper(driver);
	}

	@Test
	void navigateSuccessfully() {
		ImagesPage	imagesPage = new ImagesPage(driver, wait);
		imagesPage.navigateTo();
		assertEquals(driver.getTitle(), "Google Images", "The title is incorrect or the page is incorrect.");
	}

}
