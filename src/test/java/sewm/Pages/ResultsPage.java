package sewm.Pages;



import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
//import org.openqa.selenium.support.locators.RelativeLocator.withTagName; doesn't work.

public class ResultsPage {
	WebDriver driver;
	Wait wait;
	public ResultsPage(WebDriver driver, Wait wait){
		this.driver=driver;
		this.wait=wait;
	}
	
	public ResultsPage(WebDriver driver){
		this.driver=driver;
	}
	public int getBoldMatches(String query) { //Return number of matches in bold.
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//em[contains(text(), \'"+query+"\')]")));
		List<WebElement>  boldMatches = driver.findElements(By.xpath("//em[contains(text(), \'"+query+"\')]")); 
		return boldMatches.size();
	}
	public int getIntroMatches() { //Return number of matches with Your search.
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(), \'Your search - \')]")));
		List<WebElement>  introMatches = driver.findElements(By.xpath("//p[contains(text(), \'Your search - \')]")); 
		return introMatches.size();
	}
    public int getSuggestionMatches() { //Return number matches for "suggestions".
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(), \'Suggestions\')]")));
		List<WebElement>  suggestionMatches = driver.findElements(By.xpath("//p[contains(text(), \'Suggestions\')]")); 
		return suggestionMatches.size();
	}

    public int getSpellingSuggestions() { //Return number of spelling suggestions.
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(), \'Make sure all words are spelled correctly.\')]")));
		List<WebElement>  spellingSuggestions = driver.findElements(By.xpath("//li[contains(text(), \'Make sure all words are spelled correctly.\')]")); 
		return spellingSuggestions.size();
    }

    public int getDifferentKeywords() { //Return number of "try different keywords".
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(), \'Try different keywords.\')]")));
		List<WebElement>  differentKeywords = driver.findElements(By.xpath("//li[contains(text(), \'Try different keywords.\')]")); 
		return differentKeywords.size();
    }

    public int getGeneralKeywords() { //Return number of "try general keywords"
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains(text(), \'Try more general keywords.\')]")));
		List<WebElement>  generalKeywords = driver.findElements(By.xpath("//li[contains(text(), \'Try more general keywords.\')]")); 
		return generalKeywords.size();
    }
    
    public int getNumOfResultsBySpan(String query) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), \'"+query+"\')]")));
    	List<WebElement> results =driver.findElements(By.xpath("//span[contains(text(), \'"+query+"\')]"));
    	return results.size();
    }
    
    public int getNumOfResultsByLink(String query) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(query)));
    	List<WebElement> results =driver.findElements(By.partialLinkText(query));
    	return results.size();
    }
    
    
}