package sewm.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;

public class ImagesPage {
	WebDriver driver;
	Wait wait;
	public ImagesPage(WebDriver driver, Wait wait){
	 this.driver=driver;	
	 this.wait=wait;
	}
	
	public ImagesPage(WebDriver driver){
	 this.driver=driver;	
	}
	
	public void navigateTo() {
		WebElement imagesLink = driver.findElement(By.linkText("Images"));
		imagesLink.click();
		wait.until(ExpectedConditions.titleIs("Google Images")); //Selenium thinks wait is null.
	}

}
