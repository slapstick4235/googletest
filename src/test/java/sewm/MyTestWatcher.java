package sewm;

import java.util.Optional;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.WebDriver;

import sewm.Utils.Camera;

public class MyTestWatcher implements TestWatcher {
	
    public static WebDriver driver;
    public static String browser = System.getProperty("browser");
    @Override
    public void testFailed(ExtensionContext extensionContext, Throwable throwable) {
    String testCase = extensionContext.getRequiredTestMethod().toGenericString();
    testCase= testCase.replaceAll(
   "[^a-zA-Z]","" //Reduce string to letters.
    );
    	System.out.println(testCase);
    	Camera camera = new Camera(driver);
    	camera.screenshot(browser+testCase);
    	driver.close();
    }
    
    @Override
    public void testSuccessful(ExtensionContext extensionContext) {
    	driver.close();
    }
    
    @Override
    public void testAborted(ExtensionContext extensionContext, Throwable throwable) {
    	System.out.println("Aborted.");
    	driver.close();
    }

    @Override
    public void testDisabled(ExtensionContext extensionContext, Optional<String> optional) {
    }
    
    public static void setDriver(WebDriver driverArg) {
    	driver = driverArg;
    }
    
    
    
    

    
     
}