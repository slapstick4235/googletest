package sewm; 



import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.junit.jupiter.api.Assertions.*;

import com.deque.html.axecore.results.Rule;
import com.deque.html.axecore.selenium.*;
import com.google.common.io.Files;

import Helpers.CSSHelper;
import Helpers.TestHelper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import sewm.Pages.HomePage;
import sewm.Pages.ResultsPage;
import sewm.Utils.DriverUtility;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import org.openqa.selenium.support.Color;



//Uses the test watcher to close the browser after each test.(Extend with in superclass.)
public class HomeTest extends PageTest 
{

//The driver is inherited from PageTest, so it doesn't need to be declared here.
	String url="https://www.google.com";
	final static Color black = Color.fromString("rgb(60, 64, 67)");
	final static Color light_grey= Color.fromString("rgb(248, 249, 250)");
	final static Color grey= Color.fromString("rgb(218, 220, 224)");
	final static Color dark_blue =Color.fromString("rgb(66, 133, 244)");

	
	@BeforeEach
	void initDriver() {
		driver = utility.init(url);
		wait = utility.getWait();
		MyTestWatcher.setDriver(driver);
		this.testHelper=new TestHelper(driver);
	}

	@ParameterizedTest
	@ValueSource(strings= {"Wilson Golf", "Selenium", "War of 1812", "Back to the Future"}) //Put different queries to test in this array.
	void matchingResults(String query) {
    	HomePage home = new HomePage(driver,wait);
		String title = driver.getTitle();
		assertEquals("Google",title, "Incorrect title found for home screen.");//Title should contain query
		home.enterSearchFor(query);
		ResultsPage results = new ResultsPage(driver, wait);
		wait.until(ExpectedConditions.titleIs(query+" - Google Search"));
	    title = driver.getTitle();
		assertEquals(query+" - Google Search",title, "Incorrect title found for results screen.");//Title should contain query
		int numOfResults = results.getNumOfResultsByLink(query);
	    assertTrue(numOfResults > 0 , "Matching results haven't been found.");
	}
	


    @Test
    void accessibilityTest() 
    {
	    checkForViolations();	
    }

	@ParameterizedTest
	@ValueSource(strings= {"sdfjaklsdjfklasjklfsdkljfklasfj"})
	void noResultsTest(String query) {
		HomePage home = new HomePage(driver, wait);
		home.enterSearchFor(query);
		String title = driver.getTitle();
		ResultsPage results = new ResultsPage(driver,wait);
		wait.until(ExpectedConditions.titleIs(query+" - Google Search"));
		assertEquals(query+" - Google Search",title, "Inorrect title found for results screen.");//Title should contain query
        int boldMatches= results.getBoldMatches(query); 
		assertEquals(1, boldMatches, "Emboldened "+query+" not found.");
	    int introMatches= results.getIntroMatches();	
		assertEquals(1, introMatches, "\"Your search ... \" not found.");
        int suggestionsMatches = results.getSuggestionMatches();
		assertEquals(1, suggestionsMatches, "Suggestions not found.");
		int spellingSuggestionCount = results.getSpellingSuggestions();
		assertEquals(1 , spellingSuggestionCount, "Spelling bullet point not found.");
        int differentKeywordsCount= results.getDifferentKeywords();
		assertEquals(1,differentKeywordsCount, "Different keywords bullet point not found.");
        int generalKeywordsCount= results.getGeneralKeywords();
		assertEquals(1,generalKeywordsCount, "General keywords bullet point not found.");

	    
	}
	
    @ParameterizedTest
    @MethodSource("passiveCSSProvider")
    void passiveCSSFor(String buttonName, Color expectedBorder, Color expectedBackground, Color expectedButtonColor, String expectedTextAlign, String expectedMinWidth, String expectedHeight, String expectedPadding ) { 
    	//Passive properties
        
     	CSSHelper csshelper = new CSSHelper(driver, wait);
    	Color textColor = csshelper.getTextColorByName(buttonName);
	    assertEquals(expectedButtonColor,textColor,"The button color is not black."); //Test text color of search button
    	Color backgroundColor = csshelper.getBackgroundColorByName(buttonName);
	    assertEquals(expectedBackground, backgroundColor ,"The background color of "+buttonName+" is incorrect."); //Test background color
    	Color borderColor = csshelper.getBorderColorByName(buttonName);
	    assertEquals(expectedBorder, borderColor ,"The border color of "+buttonName+" is incorrect."); //Test border color
    	String textAlign = csshelper.getTextAlignByName(buttonName);
	    assertEquals(expectedTextAlign, textAlign ,"The text-align of "+buttonName+" is incorrect."); //Test text align 
    	String minWidth= csshelper.getMinWidthByName(buttonName);
	    assertEquals(expectedMinWidth, minWidth,"The min width of "+buttonName+" is incorrect."); //Test min width 
    	String height = csshelper.getHeightByName(buttonName);
	    assertEquals(expectedHeight, height ,"The  height of "+buttonName+" is incorrect."); //Test height
    }
    
    @ParameterizedTest
    @MethodSource("activeCSSProvider")
    void activeCSSTestFor(String buttonName, Color expectedHoverColor, Color expectedFocusColor) { 
    	//Active properties
    	List<WebElement> elements = driver.findElements(By.name(buttonName));
        WebElement activeElement = elements.get(1);
        utility.hoverOverElement(activeElement);
        CSSHelper csshelper = new CSSHelper(driver);
        Color borderColor = csshelper.getBorderColorByElement(activeElement);
	    assertEquals(expectedHoverColor, borderColor, "The border color of "+buttonName+" is incorrect when hovering over it."); //Test border color
	    activeElement.click();
	    borderColor=csshelper.getBorderColorByElement(activeElement); 
	    assertEquals(expectedFocusColor, borderColor, "The border color of "+buttonName+" is incorrect when hovering over it."); //Test border color
    }
    
    @Test
    void noSearchTest() {
		String title = driver.getTitle();
		assertTrue(title.equals("Google"), "Incorrect title found for home screen.");//Title should contain query
	    List<WebElement> searchButtons = driver.findElements(By.name("btnK"));
		searchButtons.get(1).click();
		title = driver.getTitle();
		assertTrue(title.equals("Google"), "Correct title found for results screen.");//Title should not change, since the page stays the same. 
	}

	public static Stream<Arguments> passiveCSSProvider()
    {
        return Stream.of(
                Arguments.of("btnK",light_grey,light_grey,black,"center","54px","36px", "0px 16px") 
                );

    }
   
		public static Stream<Arguments> activeCSSProvider()
		{
			return Stream.of(
                Arguments.of("btnK",grey,dark_blue) 
			);

    	}

		

}
